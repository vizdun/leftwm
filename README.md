LeftWM with a bit of *spice*. Please bear in mind that this is supposed to be a modified version of leftwm and as such is written to be as easy to update as possible as opposed to being written well.

Spice mentioned:

- broken default xmonad-style fullscreen
- clap derive
- shadow-rs version
- fullscreen shenanigans - cycling through windows while in fullscreen
- rainbow focus color