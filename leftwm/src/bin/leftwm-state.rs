use clap::{arg, command, Parser};
use leftwm_core::errors::Result;
use leftwm_core::models::dto::{DisplayState, ManagerState};
use shadow_rs::shadow;
use std::ffi::OsStr;
use std::path::{Path, PathBuf};
use std::str;
use tokio::fs;
use tokio::io::{AsyncBufReadExt, BufReader, Lines};
use tokio::net::UnixStream;
use xdg::BaseDirectories;

type Partials = liquid::partials::EagerCompiler<liquid::partials::InMemorySource>;

shadow!(build);

#[derive(Parser)]
#[command(name = "LeftWM State")]
#[command(about = "Prints out the current state of LeftWM")]
#[command(author, version, long_version = build::CLAP_LONG_VERSION, long_about = None)]
struct Args {
    /// A liquid template to use for the output
    #[arg(short, long)]
    template: Option<PathBuf>,
    /// Use a liquid template string literal to use for the output
    #[arg(short, long)]
    string: Option<String>,
    /// render only info about a given workspace [0..]
    #[arg(short, long)]
    workspace: Option<usize>,
    /// Print new lines in the output
    #[arg(short, long)]
    newline: bool,
    /// Prints the state once and quits
    #[arg(short, long)]
    quit: bool,
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();

    let mut stream_reader = stream_reader().await?;
    if let Some(path) = args.template {
        let partials = get_partials(path.parent()).await?;
        let template_str = fs::read_to_string(path).await?;
        let template = liquid::ParserBuilder::with_stdlib()
            .partials(partials)
            .build()
            .expect("Unable to build template")
            .parse(&template_str)
            .expect("Unable to parse template");
        while let Some(line) = stream_reader.next_line().await? {
            let _droppable = template_handler(&template, args.newline, args.workspace, &line);
            if args.quit {
                break;
            }
        }
    } else if let Some(string_literal) = args.string {
        let template = liquid::ParserBuilder::with_stdlib()
            .build()
            .expect("Unable to build template")
            .parse(&string_literal)
            .expect("Unable to parse template");
        while let Some(line) = stream_reader.next_line().await? {
            let _droppable = template_handler(&template, args.newline, args.workspace, &line);
            if args.quit {
                break;
            }
        }
    } else {
        while let Some(line) = stream_reader.next_line().await? {
            let _droppable2 = raw_handler(&line);
            if args.quit {
                break;
            }
        }
    }

    Ok(())
}

async fn get_partials(dir: Option<&Path>) -> Result<Partials> {
    let mut partials = Partials::empty();
    match dir {
        Some(d) => {
            let entries = fs::read_dir(d).await?;

            let partial_paths = partials_in_dir_entries(entries).await?;

            for path in partial_paths {
                partials.add(
                    path.path().as_path().to_str().unwrap_or(" "),
                    fs::read_to_string(path.path()).await?,
                );
            }
            Ok(partials)
        }
        None => Ok(partials),
    }
}

async fn partials_in_dir_entries(mut entries: fs::ReadDir) -> Result<Vec<fs::DirEntry>> {
    let mut partial_paths = vec![];
    while let Some(entry) = entries.next_entry().await? {
        let f_n = entry.file_name();
        if is_partial_filename(&f_n) {
            partial_paths.push(entry);
        }
    }
    Ok(partial_paths)
}

fn is_partial_filename(filename: &OsStr) -> bool {
    let f_n = filename.to_str().unwrap_or(" ");
    f_n.starts_with('_') && f_n.ends_with(".liquid")
}

fn raw_handler(line: &str) -> Result<()> {
    let s: ManagerState = serde_json::from_str(line)?;
    let display: DisplayState = s.into();
    let json = serde_json::to_string(&display)?;
    println!("{json}");
    Ok(())
}

fn template_handler(
    template: &liquid::Template,
    newline: bool,
    ws_id: Option<usize>,
    line: &str,
) -> Result<()> {
    let s: ManagerState = serde_json::from_str(line)?;
    let display: DisplayState = s.into();

    let globals = if let Some(ws_id) = ws_id {
        let json = serde_json::to_string(&display.workspaces[ws_id])?;
        let workspace: liquid::model::Object = serde_json::from_str(&json)?;
        let mut globals = liquid::model::Object::new();
        globals.insert(
            "window_title".into(),
            liquid::model::Value::scalar(display.window_title),
        );
        globals.insert("workspace".into(), liquid::model::Value::Object(workspace));
        globals
    } else {
        let json = serde_json::to_string(&display)?;
        let globals: liquid::model::Object = serde_json::from_str(&json)?;
        globals
    };

    let mut output = template.render(&globals).unwrap();
    output = str::replace(&output, "\r", "");
    // We use newline rather than !newline to avoid negative logic,
    // but note the difference between print! and println!. Trying to skip println!
    // will result in theme degradation, as in #263.
    if newline {
        print!("{output}");
    } else {
        output = str::replace(&output, "\n", "");
        println!("{output}");
    }
    Ok(())
}

async fn stream_reader() -> Result<Lines<BufReader<UnixStream>>> {
    let base = BaseDirectories::with_prefix("leftwm")?;
    let socket_file = base.place_runtime_file("current_state.sock")?;
    let stream = UnixStream::connect(socket_file).await?;
    Ok(BufReader::new(stream).lines())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn correctly_identifies_partial_template_filenames() {
        let file_names = vec![
            "main.liquid",
            "_partial.liquid",
            "\u{7c0}nonascii-in-filename.liquid", // first char U07C0
            "1_partial.liquid",
            "_liquid.txt",
        ];
        let partials = file_names
            .iter()
            .map(|f_n| OsStr::new(*f_n))
            .filter(|f_n| is_partial_filename(f_n))
            .collect::<Vec<&OsStr>>();

        assert!(partials == vec![OsStr::new("_partial.liquid")]);
    }
}
