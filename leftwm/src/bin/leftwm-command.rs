use anyhow::{Context, Result};
use clap::{arg, command, Parser};
use leftwm_core::CommandPipe;
use shadow_rs::shadow;
use std::fs::OpenOptions;
use std::io::prelude::*;
use xdg::BaseDirectories;

shadow!(build);

#[derive(Parser)]
#[command(name = "LeftWM Command")]
#[command(about = "Sends external commands to LeftWM")]
#[command(author, version, long_version = build::CLAP_LONG_VERSION, long_about = None)]
struct Args {
    /// Print a list of available commands with their arguments.
    #[arg(short, long)]
    list: bool,
    /// The command to be sent. See 'list' flag.
    command: Vec<String>,
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();

    let file_name = CommandPipe::pipe_name();
    let file_path = BaseDirectories::with_prefix("leftwm")?
        .find_runtime_file(&file_name)
        .with_context(|| format!("ERROR: Couldn't find {}", file_name.display()))?;
    let mut file = OpenOptions::new()
        .append(true)
        .open(file_path)
        .with_context(|| format!("ERROR: Couldn't open {}", file_name.display()))?;
    for command in args.command {
        if let Err(e) = writeln!(file, "{command}") {
            eprintln!(" ERROR: Couldn't write to commands.pipe: {e}");
        }
    }

    if args.list {
        print_commandlist();
    }
    Ok(())
}

fn print_commandlist() {
    println!(
        "
        Available Commands:

        Commands without arguments:

        UnloadTheme
        SoftReload
        ToggleFullScreen
        ToggleSticky
        SwapScreens
        MoveWindowToNextTag
        MoveWindowToPreviousTag
        MoveWindowToLastWorkspace
        MoveWindowToNextWorkspace
        MoveWindowToPreviousWorkspace
        FloatingToTile
        TileToFloating
        ToggleFloating
        MoveWindowUp
        MoveWindowDown
        MoveWindowTop
        FocusWindowUp
        FocusWindowDown
        FocusWindowTop
        FocusNextTag
        FocusPreviousTag
        FocusWorkspaceNext
        FocusWorkspacePrevious
        NextLayout
        PreviousLayout
        RotateTag
        ReturnToLastTag
        CloseWindow

        Commands with arguments:
            Use quotations for the command and arguments, like this:
            leftwm-command \"<command> <args>\"

        LoadTheme              Args: <Path_to/theme.ron>
            Note: `theme.toml` will be deprecated but stays for backwards compatibility for a while 
        AttachScratchPad       Args: <ScratchpadName>
        ReleaseScratchPad      Args: <tag_index> or <ScratchpadName>
        NextScratchPadWindow   Args: <ScratchpadName>
        PrevScratchPadWindow   Args: <ScratchpadName>
        ToggleScratchPad       Args: <ScratchpadName>
        SendWorkspaceToTag     Args: <workspaxe_index> <tag_index> (int)
        SendWindowToTag        Args: <tag_index> (int)
        SetLayout              Args: <LayoutName>
        SetMarginMultiplier    Args: <multiplier-value> (float)
        FocusWindow            Args: <WindowClass> or <visible-window-index> (int)

        For more information please visit:
        https://github.com/leftwm/leftwm/wiki/External-Commands
         "
    );
}
